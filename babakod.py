
import time
start_time = time.time()

import sys
def dec_to_bin(x):
    return str(bin(x)[2:])

N = int(sys.stdin.readline())

if not (N <= 200000 and N >= 2):
    sys.exit()

def addTree(binary, myTree):
    if len(binary) == 0:
        return myTree
    last = int(binary[-1])
    if len(binary) == 1:
        if myTree[last] == None:
            myTree[last] = [None, None, True]
        else:
            myTree[last][2] = True
        return addTree(binary[:-1],myTree[last])
    if myTree[last] == None:
        myTree[last] = [None, None, False]
    return addTree(binary[:-1],myTree[last])

def searchMinTree(binary, myTree, errorCount, currentError):
    if myTree is None:
        return False
    if(errorCount < currentError):
        return False
    lenBinary = len(binary)
    if lenBinary > 0:
        last = int(binary[-1])    
        rest = binary[:-1]
    else:
        last = 0
        rest = ""
    if(errorCount==currentError and lenBinary==0 and myTree[2]):
        return True
    elif(errorCount==currentError and last == 0 and myTree[2] and lenBinary ==0):
        return True
    elif(myTree[2] and errorCount == currentError + binary.count("1")):
        return True
    otherSide = 1
    if last == 1:
        otherSide = 0
    return searchMinTree(rest,myTree[otherSide],errorCount,currentError + 1) or searchMinTree(rest,myTree[last],errorCount,currentError)



maxLenght = 0
myArry = [set() for _ in range(21)]
myAns = []
zeros = set()
cache = {}
curTree = [None,None,False]
for i in range(0, N):
    input = sys.stdin.readline()
    input = input[:-1]
    type = int(input.split(" ")[0])
    if not (type == 1 or type == 2): sys.exit()
    number = int(input.split(" ")[1])
    if not (number >= 0 and number <= 1000000):
        sys.exit()
    binary = '{0:b}'.format(number)

    if type == 1:
        if maxLenght < len(binary):
            maxLenght = len(binary)
        addTree(binary,curTree)
        cache = {}
    elif type == 2 and i > 0:
        if binary in zeros:
            myAns.append(0)
        else:
            if number in cache.keys():
                myAns.append(cache[number])
                continue
            for error in range(0, maxLenght+1):
                if searchMinTree(binary,curTree,error,0):
                    myAns.append(error)
                    cache[number] = error
                    if error == 0:
                        zeros.add(binary)
                    break
            
    else:
        sys.exit()

for ans in myAns:
    sys.stdout.write(str(ans) + "\n")
elapsed_time = time.time() - start_time
print(elapsed_time)