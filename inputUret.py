import random

n=5000
f = open("inputs.txt", "w")
f.write(str(n)+"\n")
for i in range(n,n+n):
    f.write("1 "+str(i)+"\n")
for i in range(0,n):
    f.write(str(random.randint(1,2))+" "+str(random.randint(0,1000000))+"\n")
    # print(str(random.randint(1,2))+ " "+str(random.randint(0,1000000)))

f.close()