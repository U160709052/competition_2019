import sys

def slope(x1, x2, y2):
    m = 0
    b = (x2 - x1)
    d = (y2)
    if b != 0:
        m = (d)/(b) 

    return m
#slope(2, 3, 6, 7)

#eğimi 1 ile -1 arasında olan bütün değerleri kabul edebiliriz


n = int(sys.stdin.readline())
coords=[]
maxX=0
for i in range(0,n):
    input = sys.stdin.readline()
    input=input[:-1]
    input=input.split()
    coords=coords+[[int(input[0]),int(input[1])]]
    if maxX<int(input[0]):
        maxX=int(input[0])

maxCount=0
for x in range(1,maxX):
    count=0
    
    for point in coords:
        slopeM=slope(float(x),float(point[0]),float(point[1]))
        if slopeM>=1.0 or slopeM<=-1.0:
            count=count+1
            if slopeM==-1.0:
                deleteCoord=point
                coords.remove(deleteCoord)
        
    if count>maxCount:
        maxCount=count

sys.stdout.write(str(maxCount))


