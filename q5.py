#E den B ye yol bulup, path finder ile B to E gidiyor (Distance)


import sys
import math
import collections

def distance(p1,p2):
    a = math.sqrt( ((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2) )
    return a 


sayilar = sys.stdin.readline()
sayilar=sayilar[:-1]
veriler=str(sayilar).split()
if not( int(veriler[0])>=1 and int(veriler[0])<=20 and int(veriler[1])>=2 and int(veriler[1])<=20 and int(veriler[2])>=0 and int(veriler[2])<=10) :
    sys.exit()
yemekler=[]
yol=""
map=[]
width, height = int(veriler[1]),int(veriler[0])
wall, clear= "-", "*"
def bfs(grid, start, goal):
    queue = collections.deque([[start]])
    seen = set([start])
    while queue:
        path = queue.popleft()
        x, y = path[-1]
        if grid[y][x] == goal:
            return path
        for x2, y2 in ((x+1,y), (x-1,y), (x,y+1), (x,y-1)):
            if 0 <= x2 < width and 0 <= y2 < height and grid[y2][x2] != '^' and grid[y2][x2] != wall and (x2, y2) not in seen:
                queue.append(path + [(x2, y2)])
                seen.add((x2, y2))

for i in range(0,int(veriler[0])):
    satir = sys.stdin.readline()
    satir=satir[:-1]
    for x in range(0,len(satir)):
        if satir[x]=='B':
            Bcoor=[x,i]
        if satir[x]=='E':
            Ecoor=[x,i]
        if satir[x] in "0123456789":
            yemekler.append([x,i])
    map.append(satir)
tempE=Ecoor
expectedPath=[]
expectedPath.append('E')
for count in range(0,int(veriler[2])):
    close=50
    for i in yemekler:
        distances=distance(i,tempE)
        if int(distances)<int(close):
            closer=i
            close=distances
    expectedPath.append(map[closer[1]][closer[0]])
    tempE=closer
    if count<int(veriler[2]):
        yemekler.remove(closer)
expectedPath.reverse()
defPath=[]
tempCoorB=Bcoor
for target in expectedPath:
    defPath= defPath+ list(bfs(map, tuple(tempCoorB), target))
    tempCoorB=list(tempCoorB)
    tempCoorB[0]=defPath[-1][0]
    tempCoorB[1]=defPath[-1][1]
    tempCoorB=tuple(tempCoorB)

result=""
for point in defPath[1:]:
    if Bcoor[0]>point[0]:
        result=result+"W"
    elif Bcoor[0]<point[0]:
        result=result+"E"
    elif Bcoor[1]<point[1]:
        result=result+"S"
    elif Bcoor[1]>point[1]:
        result=result+"N"
    Bcoor=point

sys.stdout.write(result)
