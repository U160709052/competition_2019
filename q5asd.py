#Olası yolların permütasyonunu belirleyip en kısa yolu buluyor

import sys
import math
import collections
import itertools

def distance(p1,p2):
    a = math.sqrt( ((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2) )
    return a 


sayilar = sys.stdin.readline()
sayilar=sayilar[:-1]
veriler=str(sayilar).split()
width, height = int(veriler[1]),int(veriler[0])
if not( int(veriler[0])>=1 and int(veriler[0])<=20 and int(veriler[1])>=2 and int(veriler[1])<=20 and int(veriler[2])>=0 and int(veriler[2])<=10) :
    sys.exit()
yemekler=[]
map=[]
for i in range(0,int(veriler[0])):
    satir = sys.stdin.readline()
    satir=satir[:-1]
    for x in range(0,len(satir)):
        if satir[x]=='B':
            Bcoor=(x,i)
        if satir[x]=='E':
            Ecoor=(x,i)
        if satir[x] in "0123456789":
            yemekler.append(satir[x])
    map.append(satir)

muhtemelDuraklar=list(itertools.permutations(yemekler,int(veriler[2])))
#print(muhtemelDuraklar)
wall, clear= "-", "*"
def bfs(grid, start, goal):
    queue = collections.deque([[start]])
    seen = set([start])
    while queue:
        path = queue.popleft()
        x, y = path[-1]
        if grid[y][x] == goal:
            return path
        for x2, y2 in ((x+1,y), (x-1,y), (x,y+1), (x,y-1)):
            if 0 <= x2 < width and 0 <= y2 < height and grid[y2][x2] != '^' and grid[y2][x2] != wall and (x2, y2) not in seen:
                queue.append(path + [(x2, y2)])
                seen.add((x2, y2))

min_len=10000
Cresult=[]
count=0
for duraklar in muhtemelDuraklar:
    path2=[]
    current=Bcoor
    for durak in duraklar:
        path2= path2+ list(bfs(map, Bcoor, durak))
        current=list(current)
        current[0]=path2[-1][0]
        current[1]=path2[-1][1]
        current=tuple(current)   
    path2= path2+ list(bfs(map, current, 'E'))
    if len(path2)<=min_len:
        Cresult=path2
        min_len=len(Cresult)
    count = count +1
    #print(count)
begin=Cresult[0]
result=""
for point in Cresult[1:]:
    if begin[0]>point[0]:
        result=result+"W"
    elif begin[0]<point[0]:
        result=result+"E"
    elif begin[1]<point[1]:
        result=result+"S"
    elif begin[1]>point[1]:
        result=result+"N"
    begin=point
    
sys.stdout.write(str(result))
