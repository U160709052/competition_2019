#B den başlayarak distance lara göre E ye giden yolu buluyor

import sys
import math
import collections

def distance(p1,p2):
    a = math.sqrt( ((p1[0]-p2[0])**2)+((p1[1]-p2[1])**2) )
    return a 

    
sayilar = sys.stdin.readline()
sayilar=sayilar[:-1]
veriler=str(sayilar).split()
width, height = int(veriler[1]),int(veriler[0])
if not( int(veriler[0])>=1 and int(veriler[0])<=20 and int(veriler[1])>=2 and int(veriler[1])<=20 and int(veriler[2])>=0 and int(veriler[2])<=10) :
    sys.exit()
yemekler=[]
map=[]
for i in range(0,int(veriler[0])):
    satir = sys.stdin.readline()
    satir=satir[:-1]
    for x in range(0,len(satir)):
        if satir[x]=='B':
            Bcoor=(x,i)
        if satir[x]=='E':
            Ecoor=(x,i)
        if satir[x] in "0123456789":
            yemekler=yemekler + [(x,i)]
    map.append(satir)
path2=[]
yemekLen= len(yemekler)
wall, clear= "-", "*"
def bfs(grid, start, goal):
    queue = collections.deque([[start]])
    seen = set([start])
    while queue:
        path = queue.popleft()
        x, y = path[-1]
        if grid[y][x] == goal:
            return path
        for x2, y2 in ((x+1,y), (x-1,y), (x,y+1), (x,y-1)):
            if 0 <= x2 < width and 0 <= y2 < height and grid[y2][x2] != '^' and grid[y2][x2] != wall and (x2, y2) not in seen:
                queue.append(path + [(x2, y2)])
                seen.add((x2, y2))
mainCounter = 0
for count in range(0,int(veriler[2])+1):
    
    if count==int(veriler[2]):
       closer=Ecoor
    else:
        for i in yemekler:
            close=50
            distances=distance(i,Bcoor)
            if int(distances)<int(close):
                closer=i
                close=distances

    pointer=map[closer[1]][closer[0]]
    path2= path2+ list(bfs(map, Bcoor, pointer))
    mainCounter = mainCounter +1
    Bcoor=list(Bcoor)
    Bcoor[0]=closer[0]
    Bcoor[1]=closer[1]
    Bcoor=tuple(Bcoor)


    if count<int(veriler[2]):
        yemekler.remove(closer)
if mainCounter == yemekLen +1 :
    begin=path2[0]
    result=""
    for point in path2[1:]:
        if begin[0]>point[0]:
            result=result+"W"
        elif begin[0]<point[0]:
            result=result+"E"
        elif begin[1]<point[1]:
            result=result+"S"
        elif begin[1]>point[1]:
            result=result+"N"
        begin=point
    sys.stdout.write(str(result))
