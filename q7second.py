import sys

def possibleWay(col_combi):
    common_cols = []
    common_group_count = 0

    for (c1, c2) in col_combi:
        found = False
        for i in range(len(common_cols)):
            if (c1 in common_cols[i]):
                common_cols[i].append(c2)
                found = True
                break
            elif (c2 in common_cols[i]):
                common_cols[i].append(c1)
                found = True
                break
        if not found:
            common_cols.append([c1,c2])
    return common_cols

toplamlar = sys.stdin.readline()
toplamlar=toplamlar[:-1]
toplamlar=toplamlar.split()
ts=int(toplamlar[0])
bridge=int(toplamlar[1])

if ts>=2 and ts<=50 and bridge>=0 and bridge<=1000:
    kuleler=[]
    for i in range(0,ts-2):
        input = sys.stdin.readline()
        input = input[:-1]
        input=input.split()
        kuleler=kuleler+[[int(input[0]),int(input[1])]]
    kopruler=[]
    for i in range(0,bridge):
        input = sys.stdin.readline()
        input = input[:-1]
        input=input.split()
        kopruler=kopruler+[[input[0],input[1]]]

print(possibleWay(kopruler))