import sys

def dec_to_bin(x):
    return str(bin(x)[2:])

def xor(a, b,min):
    count=0
    for i in range(0,len(a)):
        if a[i]!=b[i]:
            count=count+1
            if count>=min:
                return count
    return count

N = int(sys.stdin.readline())

if not (N <= 200000 and N >= 2):
    sys.exit()
myArry = [set() for _ in range(21)]
myAns = []
for i in range(0, N):
    input = sys.stdin.readline()
    input = input[:-1]
    type = int(input.split(" ")[0])
    if not (type == 1 or type == 2): sys.exit()
    number = int(input.split(" ")[1])
    if not (number >= 0 and number <= 1000000):
        sys.exit()
    binary = '{0:b}'.format(number)
    myLen = len(binary)
    if type == 1:
        if binary not in myArry[myLen]:
            myArry[myLen].add(binary)
    elif type == 2 and i > 0:
        min = 20
        for index in range(0, 21):
            if min<=1:
                break
            if myLen - index >= 0:
                front = binary[0: index].count("1")
                if not (front >= min):
                    for small in myArry[myLen - index]:
                        result = xor(binary[index:], small,min-front) + front
                        if result < min:
                            min = result
                            if min == 0:
                                break
                            elif min==1 and index>0:
                                break

            if min<=1:
                break
            if index == 0:
                continue
            if index + myLen <= 20:
                for big in myArry[myLen + index]:
                    front = big[0:index].count("1")
                    if not(front >= min):
                        result = xor(binary, big[index:],min-front) + front
                        if result < min:
                            min = result
                            if min<=1:
                                break
        myAns.append(min)
    else:
        sys.exit()

for ans in myAns:
    sys.stdout.write(str(ans) + "\n")