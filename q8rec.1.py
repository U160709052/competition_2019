import  itertools
maxLen = 0
def addTree(binary, myTree):
    if len(binary) == 0:
        return myTree
    last = int(binary[-1])
    if len(binary) == 1:
        if myTree[last] == None:
            myTree[last] = [None, None, True]
        else:
            myTree[last][2] = True
        return addTree(binary[:-1],myTree[last])
    if myTree[last] == None:
        myTree[last] = [None, None, False]
    return addTree(binary[:-1],myTree[last])

# 11111101110
#       11110
# 10011001110


def searchMinTree(binary, myTree, currentError):
    if myTree is None:
        return 20
    lenBinary = len(binary)
    if lenBinary > 0:
        last = int(binary[-1])    
        rest = binary[:-1]
    else:
        last = 0
        rest = ""
    otherSide = 1
    if last == 1:
        otherSide = 0
    errorA = searchMinTree(rest,myTree[otherSide],currentError + 1) 
    errorB = searchMinTree(rest,myTree[last],currentError)
    result = []
    result.append(errorA)
    result.append(errorB)
    if(lenBinary==0 and myTree[2]):
        result.append(currentError)
        result.sort()
        return result[0]
    elif(last == 0 and myTree[2] and lenBinary ==0):
        result.append(currentError)
        result.sort()
        return result[0]
    elif(myTree[2]):
        result.append(currentError + binary.count("1"))
        result.sort()
        return result[0]
    result.sort()
    return result[0]



curTree = [None, None, False]

addTree("10",curTree)
print(searchMinTree("1",curTree,0))
addTree("1",curTree)
print(searchMinTree("11",curTree,0))





