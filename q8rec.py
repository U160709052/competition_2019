import  itertools
maxLen = 0
def addTree(binary, myTree):
    if len(binary) == 0:
        return myTree
    last = int(binary[-1])
    if len(binary) == 1:
        if myTree[last] == None:
            myTree[last] = [None, None, True]
        else:
            myTree[last][2] = True
        return addTree(binary[:-1],myTree[last])
    if myTree[last] == None:
        myTree[last] = [None, None, False]
    return addTree(binary[:-1],myTree[last])

# 11111101110
#       11110
# 10011001110

# 1110


def searchMinTree(binary, myTree, errorCount, currentError):
    if myTree is None:
        return False
    if(errorCount < currentError):
        return False
    lenBinary = len(binary)
    if lenBinary > 0:
        last = int(binary[-1])    
        rest = binary[:-1]
    else:
        last = 0
        rest = ""
    if(errorCount==currentError and lenBinary==0 and myTree[2]):
        return True
    elif(errorCount==currentError and last == 0 and myTree[2] and lenBinary ==0):
        return True
    elif(myTree[2] and errorCount == currentError + binary.count("1")):
        return True
    otherSide = 1
    if last == 1:
        otherSide = 0
    return searchMinTree(rest,myTree[otherSide],errorCount,currentError + 1) or searchMinTree(rest,myTree[last],errorCount,currentError)




curTree = [None, None, False]

addTree("1110001011100101011",curTree) 
addTree("1110110011100101011",curTree)
addTree("1110111011100111011",curTree)
addTree("1110111011000101011",curTree)
addTree("1110111011101101011",curTree)
addTree("1110111011110101011",curTree)
addTree("1100111011100101011",curTree)
addTree("1110011011100101011",curTree)
find=False
for error in range(0,21):
    if searchMinTree("1011100101011",curTree,error,0):
        print(error)
        find=True
        break 

